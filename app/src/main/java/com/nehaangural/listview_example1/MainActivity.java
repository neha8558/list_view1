package com.nehaangural.listview_example1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {
    ListView listView;
    String[]fruitName={"Mango","Apple","Banana","Grapes","strawberry","Mango","Apple","Banana","Grapes","strawberry"};

    String[]price={"$7/kg","$8/kg","$9/kg","$4/kg","$4/kg","$7/kg","$8/kg","$9/kg","$4/kg","$4/kg"};
    String[]description={"$3.50","$4.50","$5.70","$4.50","$5.90","$3.50","$4.50","$5.70","$4.50","$5.90"};
    int[] images={R.drawable.mangoo,R.drawable.applee,R.drawable.banana,R.drawable.grapes,R.drawable.strawberry,R.drawable.mangoo,R.drawable.applee,R.drawable.banana,R.drawable.grapes,R.drawable.strawberry};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView=findViewById(R.id.listView);
        customAdapter customAdapter=new customAdapter(fruitName,price,description,images);
        listView.setAdapter(customAdapter);
    }
}