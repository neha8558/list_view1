package com.nehaangural.listview_example1;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class customAdapter extends BaseAdapter {
    String[] fruitName,price,description;
    int[] image;
    public customAdapter(String[] fruitName,String[] price,String[] description,int images[]) {
        this.fruitName=fruitName;
        image=images;
        this.price=price;
        this.description=description;
    }
    @Override
    public int getCount() {
        return image.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View vieww= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item,viewGroup,false);
        TextView textView=vieww.findViewById(R.id.fruitName);
        TextView textView1=vieww.findViewById(R.id.price);
        TextView textView2=vieww.findViewById(R.id.description);
        ImageView imageView=vieww.findViewById(R.id.icon);
        textView.setText(fruitName[position]);
        textView1.setText(price[position]);
        textView2.setText(description[position]);
        imageView.setImageResource(image[position]);
        return vieww;
    }
}